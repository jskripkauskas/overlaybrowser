-dontobfuscate
-optimizationpasses 5

#-------------BUTTERKNIFE--------------
-dontwarn butterknife.internal.**
-keep class **$$ViewInjector { *; }
-keepnames class * { @butterknife.InjectView *;}

-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

#--------------DATABINDING-------------
 -dontwarn android.databinding.**
 -keep class android.databinding.** { *; }
 -dontwarn android.databinding.tool.util.**

#-------------GSON-------------------
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

#-----------REGULAR+APPENGINE---------

 -keepclasseswithmembernames class * {
     native <methods>;
 }

 -keepclasseswithmembers class * {
     public <init>(android.content.Context, android.util.AttributeSet);
 }

 -keepclasseswithmembers class * {
     public <init>(android.content.Context, android.util.AttributeSet, int);
 }

 -keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
 }

 -keepclassmembers enum * {
     public static **[] values();
     public static ** valueOf(java.lang.String);
 }

 -keep class * implements android.os.Parcelable {
   public static final android.os.Parcelable$Creator *;
 }

 # Needed by google-api-client to keep generic types and @Key annotations accessed via reflection

 -keepclassmembers class * {
   @com.google.api.client.util.Key <fields>;
 }

 -keepattributes Signature,RuntimeVisibleAnnotations,AnnotationDefault

 # Needed by Guava

 -dontwarn sun.misc.Unsafe

 # See https://groups.google.com/forum/#!topic/guava-discuss/YCZzeCiIVoI
 -dontwarn com.google.common.collect.MinMaxPriorityQueue

 #---------------RXJAVA--------------

 -dontwarn sun.misc.**

 -keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
 }
 -keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
     rx.internal.util.atomic.LinkedQueueNode producerNode;
 }
 -keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
     rx.internal.util.atomic.LinkedQueueNode consumerNode;
 }

 -keep class rx.schedulers.Schedulers {
     public static <methods>;
 }
 -keep class rx.schedulers.ImmediateScheduler {
     public <methods>;
 }
 -keep class rx.schedulers.TestScheduler {
     public <methods>;
 }
 -keep class rx.schedulers.Schedulers {
     public static ** test();
 }
 -keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
     long producerNode;
     long consumerNode;
 }

-dontwarn javax.annotation.**
-dontwarn javax.inject.**

#------------OKHTTP--------------
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

# Okio
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

#------------RETROLAMBDA---------------

-dontwarn java.lang.invoke.*

#--------------------------------

-dontwarn android.util.*
-dontwarn com.google.android.gms.auth.*
-dontwarn com.google.common.**
-dontwarn com.google.api.client.json.jackson2.JacksonFactory
-dontwarn android.app.Notification
-dontwarn org.apache.commons.io.**
-dontwarn com.google.android.gms.common.AccountPicker

#----------EVENTBUS-------------
-keepclassmembers class ** {
    public void onEvent(**);
}

-keepclassmembers class ** {
    public void onEventMainThread(**);
}

#----------RETROFIT-------------


-keep class com.squareup.okhttp.** { *; }
-keep class retrofit.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn retrofit.**
-dontwarn rx.**

-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

# If in your rest service interface you use methods with Callback argument.
-keepattributes Exceptions

# If your rest service methods throw custom exceptions, because you've defined an ErrorHandler.
-keepattributes Signature