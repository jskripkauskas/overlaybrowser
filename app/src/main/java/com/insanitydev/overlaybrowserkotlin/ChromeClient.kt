package com.insanitydev.overlaybrowserkotlin

import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.overlay.view.*

object ChromeClient : WebChromeClient() {
    override fun onProgressChanged(view: WebView, progress: Int) {
        if (OverlayWebList.size == 0){
            return
        }
        if (progress < 100 && OverlayWebList.getCurrent().webLoadProgress.visibility === ProgressBar.GONE) {
            OverlayWebList.getCurrent().webLoadProgress.visibility = ProgressBar.VISIBLE
        }

        OverlayWebList.getCurrent().webLoadProgress.progress = progress
        if (progress == 100) {
            OverlayWebList.getCurrent().webLoadProgress.visibility = ProgressBar.GONE
        }
    }
}