package com.insanitydev.overlaybrowserkotlin

import android.content.Context
import android.graphics.*
import android.support.v7.widget.LinearLayoutManager
import android.util.DisplayMetrics
import android.util.Patterns
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.util.SparseArray
import kotlinx.android.synthetic.main.head.view.*
import kotlinx.android.synthetic.main.overlay.view.*
import kotlinx.android.synthetic.main.tabs.view.*
import java.util.*
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


/**
 * Created by Julius.
 */
open class HeadLayer(mContext: Context) : View(mContext), ItemTouchHelperAdapter {
    protected lateinit var mFrameLayout: FrameLayout
    protected var headView: View? = null
    protected var mWindowManager: WindowManager? = null
    protected var layoutInflater: LayoutInflater? = null
    protected var params: WindowManager.LayoutParams? = null
    protected var screenshots: SparseArray<Bitmap> = SparseArray()
    protected var tabsView: View

    protected val backListener = OnClickListener { OverlayWebList.getCurrent().webView.goBack() }

    protected val tabBackgroundClickListener = OnClickListener { v ->
        collapse()
        OverlayWebList.currentTab = (v.parent as View).tag as Int
        expand()
    }

    protected val tabCloseClickListener = OnClickListener { v ->
        val selected = (v.parent.parent as View).tag as Int
        onItemDismiss(selected)
    }

    protected val tabsListener = OnClickListener {
        collapse()
        updateWindowInputAllowed()
        tabsView = layoutInflater!!.inflate(R.layout.tabs, mFrameLayout, false)
        mFrameLayout.addView(tabsView)
        val data = ArrayList<TabViewModel>()

        for (i in 0..screenshots.size() - 1) {
            val key = screenshots.keyAt(i)
            val screenshot = screenshots.get(key)
            val item: TabViewModel = TabViewModel(TabItem(screenshot, OverlayWebList[i].webView.title))
            item.tabBackgroundClickListener = tabBackgroundClickListener
            item.tabCloseClickListener = tabCloseClickListener
            data.add(item)
        }
        val adapter = SnappingAdapter(data)
        headView?.visibility = View.GONE
        tabsView.snappingRecycler.addItemDecoration(RecyclerSpacingDecoration(context.resources
                .getDimensionPixelOffset(R.dimen.tab_item_spacing)))
        val dismissHelper = DismissItemTouchCallback(this)
        val itemTouchHelper = ItemTouchHelper(dismissHelper)
        itemTouchHelper.attachToRecyclerView(tabsView.snappingRecycler)
        tabsView.snappingRecycler.layoutManager = LinearLayoutManager(context)
        tabsView.snappingRecycler.adapter = adapter
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean = false

    override fun onItemDismiss(position: Int) {
        val adapter: SnappingAdapter = tabsView.snappingRecycler.adapter as SnappingAdapter
        val removedWebView = OverlayWebList.removeAt(position)

        if (removedWebView.webView != null && removedWebView.webView.url != null) {
            Session.remove(removedWebView.webView.url).save(context.applicationContext)
        }
        screenshots.removeAt(position)
        adapter.remove(position)
        //shift everything
        val tempHashMap = SparseArray<Bitmap>()
        for (i: Int in 0..screenshots.size() - 1) {
            tempHashMap.put(i, screenshots.get(screenshots.keyAt(i)))
        }
        screenshots = tempHashMap
        adapter.notifyDataSetChanged()

        if (position <= OverlayWebList.currentTab) {
            OverlayWebList.currentTab--
            if (OverlayWebList.currentTab < 0 && OverlayWebList.size > 0) {
                OverlayWebList.currentTab = 0
            } else if (OverlayWebList.currentTab < 0) {
                createOverlayView()
                collapse()
                expand()
            }
        }
        renewTabCount()
    }

    protected val menuClickListener = PopupMenu.OnMenuItemClickListener { item ->
        when (item?.itemId) {
            R.id.menuNewTab -> {
                collapse()
                createOverlayView()
                expand()
                renewTabCount()
                true
            }
            R.id.menuDesktop -> {
                val webView: ObservableWebView = OverlayWebList.getCurrent().webView
                webView.setDesktopMode(!webView.settings.useWideViewPort)
                webView.settings.displayZoomControls = false
                webView.reload()
                true
            }
            else -> false
        }
    }

    protected val showMenuListener = OnClickListener { view ->
        val popup = PopupMenu(context, view)
        popup.setOnMenuItemClickListener(menuClickListener)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.browser_menu, popup.menu)
        popup.menu.findItem(R.id.menuDesktop).isChecked = OverlayWebList.getCurrent()
                .webView.settings.useWideViewPort
        popup.show()
    }

    protected val touchListener = object : View.OnTouchListener {
        protected var initX: Int = 0
        protected var initY: Int = 0
        protected var initTouchX: Int = 0
        protected var initTouchY: Int = 0
        protected val MAX_CLICK_DURATION = 200
        protected var startClickTime: Long = 0

        override fun onTouch(v: View, event: MotionEvent): Boolean {
            val x = event.rawX.toInt()
            val y = event.rawY.toInt()

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    initX = params!!.x
                    initY = params!!.y
                    initTouchX = x
                    initTouchY = y
                    startClickTime = Calendar.getInstance().timeInMillis
                    return true
                }

                MotionEvent.ACTION_UP -> {
                    val clickDuration = Calendar.getInstance().timeInMillis - startClickTime
                    if (clickDuration < MAX_CLICK_DURATION) {
                        expand()
                    }
                    return true
                }

                MotionEvent.ACTION_MOVE -> {
                    params!!.x = initX + (x - initTouchX)
                    params!!.y = initY + (y - initTouchY)
                    // Invalidate layout
                    mWindowManager!!.updateViewLayout(mFrameLayout, params)
                    saveHeadLocation()
                    return true
                }
            }
            return false
        }
    }

    internal var editorActionListener: TextView.OnEditorActionListener = TextView.OnEditorActionListener { v, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            var url = v.text.toString()
            val fixedUrl = if (!url.contains("http://")) "http://" + url else url

            if (!Patterns.WEB_URL.matcher(fixedUrl).matches()) {
                url = "https://www.google.com/search?q=" + url
            } else {
                url = fixedUrl
            }
            val webView: ObservableWebView = OverlayWebList.getCurrent().webView
            webView.loadUrl(url)
            webView.requestFocus()
            return@OnEditorActionListener true
        }
        return@OnEditorActionListener false
    }

    init {
        mWindowManager = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        layoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        mFrameLayout = object : FrameLayout(ContextThemeWrapper(context, R.style.AppTheme)) {
            override fun dispatchKeyEvent(event: KeyEvent): Boolean {
                if (event.keyCode === KeyEvent.KEYCODE_BACK) {
                    collapse()
                }
                return super.dispatchKeyEvent(event)
            }
        }

        Session.restore = false
        tabsView = layoutInflater!!.inflate(R.layout.tabs, mFrameLayout, false)
        createOverlayView()
        addToWindowManager()
    }

    protected fun renewTabCount() {
        for (overlayWebView in OverlayWebList) {
            overlayWebView.tabsBtn.text = "${OverlayWebList.size}"
        }
    }

    fun restoreSession() {
        Session.restore = true
        val urlList = Session.getSavedSession(context.applicationContext)

        if (urlList.size > 0) {
            OverlayWebList.clear()
            screenshots.clear()
            val adapter = tabsView.snappingRecycler.adapter as SnappingAdapter?
            adapter?.clear()
        }

        for (url in urlList) {
            retrieveScreenshotFromCache(url, urlList)
            createOverlayView()
            OverlayWebList.getCurrent().webView.loadUrl(url)
            renewTabCount()
        }
    }

    protected fun retrieveScreenshotFromCache(url: String, urlList: MutableList<String>) {
        try {
            val cacheDir = File(context.cacheDir, "tabs")
            val cacheFile = File(cacheDir, url.hashCode().toString())
            val fileInputStream = FileInputStream(cacheFile)
            screenshots.put(urlList.indexOf(url), BitmapFactory.decodeStream(fileInputStream))
        } catch (ex: Exception) {
            retrieveScreenshotFromCache("empty", urlList)
        }
    }

    protected fun createOverlayView() {
        OverlayWebList.add(layoutInflater!!.inflate(R.layout.overlay, mFrameLayout, false))
        val metrics = DisplayMetrics()
        mWindowManager!!.defaultDisplay.getMetrics(metrics)
        val paramsView = FrameLayout.LayoutParams(metrics.widthPixels, metrics.heightPixels)
        OverlayWebList.currentTab = OverlayWebList.lastIndex
        val overlayWeb = OverlayWebList.getCurrent()
        overlayWeb.layoutParams = paramsView
        setupWebView(overlayWeb.webView)

        overlayWeb.tabsBtn.setOnClickListener(tabsListener)
        overlayWeb.backBtn.setOnClickListener(backListener)
        overlayWeb.rippleBackBtn.visibility = if (overlayWeb.webView.canGoBack()) View.VISIBLE else View.GONE
        overlayWeb.moreBtn.setOnClickListener(showMenuListener)
        overlayWeb.urlEdit.setOnEditorActionListener(editorActionListener)
    }

    protected fun addToWindowManager() {
        params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        or WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT)
        params!!.gravity = Gravity.START
        mWindowManager!!.addView(mFrameLayout, params)

        // Here is the place where you can inject whatever layout you want.
        headView = layoutInflater?.inflate(R.layout.head, mFrameLayout)?.imageView

        // Support dragging the image view
        mFrameLayout.imageView.setOnTouchListener(touchListener)
        headView?.post { retrieveHeadLocation(true) }
    }

    fun expand() {
        //update params for input
        updateWindowInputAllowed()
        headView?.visibility = View.GONE

        if (OverlayWebList.getCurrent().parent == null) {
            mFrameLayout.addView(OverlayWebList.getCurrent())
        }
    }

    fun collapse() {
        updateWindowInputDisabled()
        headView?.visibility = View.VISIBLE
        saveScreenshot(OverlayWebList.currentTab)
        mFrameLayout.removeView(if (tabsView.isShown) tabsView else OverlayWebList.getCurrent())
        retrieveHeadLocation(false)
    }

    protected fun retrieveHeadLocation(restore: Boolean) {
        val prefs = context.getSharedPreferences("OverlayBrowser", Context.MODE_PRIVATE)
        val metrics = DisplayMetrics()
        mWindowManager!!.defaultDisplay.getMetrics(metrics)
        params!!.x = prefs.getInt("headX", 0) - if (!restore) metrics.widthPixels / 2 - headView!!.width / 2 else 0
        params!!.y = prefs.getInt("headY", 0) - metrics.heightPixels / 2 + headView!!.height / 4
        // Invalidate layout
        mWindowManager!!.updateViewLayout(mFrameLayout, params)
    }

    protected fun saveHeadLocation() {
        val prefs = context.getSharedPreferences("OverlayBrowser", Context.MODE_PRIVATE)
        val viewLocation = IntArray(2)
        headView?.getLocationOnScreen(viewLocation)
        prefs.edit().putInt("headX", viewLocation[0]).putInt("headY", viewLocation[1]).apply()
    }

    fun saveScreenshot(tabIndex: Int) {
        val bitmap = loadBitmapFromView(OverlayWebList.getCurrent().webView)
        screenshots.put(tabIndex, bitmap)
        val urlString = OverlayWebList.getCurrent().webView.url
        putBitmapInDiskCache(urlString ?: "empty", bitmap)
    }

    fun loadBitmapFromView(v: View): Bitmap {
        val metrics = DisplayMetrics()
        mWindowManager!!.defaultDisplay.getMetrics(metrics)
        val imgWidth = v.layoutParams.width
        val imgHeight = v.layoutParams.height
        val b = Bitmap.createBitmap(if (imgWidth < 1) metrics.widthPixels else imgWidth,
                if (imgHeight < 1) metrics.heightPixels - OverlayWebList.getCurrent().urlEdit.height
                else imgHeight, Bitmap.Config.ARGB_8888)
        val bitmap = resize(b, metrics.widthPixels / 2, metrics.heightPixels / 2)
        val c = Canvas(bitmap)
        c.scale(0.5f, 0.5f)
        v.layout(v.left, v.top, v.right, v.bottom)
        v.draw(c)
        return bitmap
    }

    fun putBitmapInDiskCache(url: String, avatar: Bitmap) {
        val cacheDir = File(context.cacheDir, "tabs")
        if (!cacheDir.exists()) {
            cacheDir.mkdir()
        }
        val cacheFile = File(cacheDir, "" + url.hashCode())
        try {
            cacheFile.createNewFile()
            val fos = FileOutputStream(cacheFile)
            avatar.compress(CompressFormat.PNG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun resize(image: Bitmap, maxWidth: Int, maxHeight: Int): Bitmap {
        if (maxHeight > 0 && maxWidth > 0) {
            val width = image.width
            val height = image.height
            val ratioBitmap = width.toFloat() / height.toFloat()
            val ratioMax = maxWidth.toFloat() / maxHeight.toFloat()

            var finalWidth = maxWidth
            var finalHeight = maxHeight
            if (ratioMax > 1) {
                finalWidth = (maxHeight.toFloat() * ratioBitmap).toInt()
            } else {
                finalHeight = (maxWidth.toFloat() / ratioBitmap).toInt()
            }
            return Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true)
        } else {
            return image
        }
    }

    fun isExpanded(): Boolean = OverlayWebList.getCurrent().isShown || tabsView.isShown

    protected fun setupWebView(webView: ObservableWebView) {
        webView.setWebViewClient(WebClient)
        webView.setWebChromeClient(ChromeClient)

        val resources = resources
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (resourceId > 0) {
            val params = webView.layoutParams as RelativeLayout.LayoutParams
            params.bottomMargin = resources.getDimensionPixelSize(resourceId) / 3
            webView.invalidate()
        }
    }

    protected fun updateWindowInputDisabled() {
        try {
            //update params to remove input
            params = WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            or WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT)
            mWindowManager!!.updateViewLayout(mFrameLayout, params)
        } catch (ex: IllegalArgumentException) {
            addToWindowManager()
        }
    }

    protected fun updateWindowInputAllowed() {
        //update params for input
        params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT)
        mWindowManager!!.updateViewLayout(mFrameLayout, params)
    }

    /**
     * Removes the view from window manager.
     */
    fun destroy() {
        mFrameLayout.removeAllViews()
        screenshots.clear()
        layoutInflater = null
        params = null
        headView = null
        OverlayWebList.clear()
        Session.restore = false
        mWindowManager!!.removeView(mFrameLayout)
        mWindowManager = null
    }
}