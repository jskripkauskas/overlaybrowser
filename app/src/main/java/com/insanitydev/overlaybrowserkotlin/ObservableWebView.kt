package com.insanitydev.overlaybrowserkotlin

import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView
import im.delight.android.webview.AdvancedWebView

/**
 * Created by Julius.
 */
class ObservableWebView : AdvancedWebView {
    var onScrollChangedCallback: OnScrollChangedCallback? = null

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        if (onScrollChangedCallback != null) onScrollChangedCallback!!.onScroll(l, t)
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    interface OnScrollChangedCallback {
        fun onScroll(l: Int, t: Int)
    }
}