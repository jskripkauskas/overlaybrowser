package com.insanitydev.overlaybrowserkotlin

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class RecyclerSpacingDecoration : RecyclerView.ItemDecoration {
    private var space: Int = 0
    var isHorizontal = true

    constructor(space: Int) {
        this.space = space
    }

    constructor(space: Int, horizontal: Boolean) {
        this.space = space
        this.isHorizontal = horizontal
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        if (isHorizontal) {
            outRect.left = space
            outRect.right = space
        }
        outRect.bottom = space
        outRect.top = space
    }
}