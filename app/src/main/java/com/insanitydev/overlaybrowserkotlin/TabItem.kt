package com.insanitydev.overlaybrowserkotlin

import android.graphics.Bitmap

class TabItem(val screenshot: Bitmap, val title: String = "New Tab")
