package com.insanitydev.overlaybrowserkotlin

import android.content.Context
import com.google.gson.Gson
import java.util.*
import com.google.gson.reflect.TypeToken

object History {
    val gson = Gson()
    var urlList: ArrayList<HistoryItem> = ArrayList()

    fun add(historyItem: HistoryItem): History {
        urlList.add(historyItem)
        return this
    }

    fun remove(historyItem: HistoryItem): History {
        urlList.remove(historyItem)
        return this
    }

    fun save(context: Context) {
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayHistoryPrefs), Context.MODE_PRIVATE)
        prefs.edit().putString(context.getString(R.string.historyKey), gson.toJson(urlList)).apply()
    }

    fun getSavedHistory(context: Context): ArrayList<HistoryItem> {
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayHistoryPrefs), Context.MODE_PRIVATE)
        val savedList = prefs.getString(context.getString(R.string.historyKey), "")
        if (savedList == ""){
            return urlList
        }

        val listType = object : TypeToken<List<HistoryItem>>() {}.type
        urlList = Gson().fromJson<List<HistoryItem>>(savedList, listType) as ArrayList<HistoryItem>
        return urlList
    }

    fun clearSavedHistory(context: Context) {
        urlList.clear()
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayHistoryPrefs), Context.MODE_PRIVATE)
        prefs.edit().clear().apply()
    }
}