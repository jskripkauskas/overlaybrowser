package com.insanitydev.overlaybrowserkotlin

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.fastaccess.permission.base.PermissionHelper
import com.fastaccess.permission.base.callback.OnPermissionCallback
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : AppCompatActivity(), OnPermissionCallback {
    protected var started = false

    protected val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == HeadService.START || intent?.action == HeadService.NOTIFY_STATE) {
                showBtn.setText(R.string.disable_overlay)
                started = true
            } else if (intent?.action == HeadService.STOP) {
                showBtn.setText(R.string.enable_overlay)
                started = false
            }
        }
    }

    protected val showButtonClick: View.OnClickListener = View.OnClickListener {
        val permissionHelper = PermissionHelper.getInstance(this)
        val currentApiVersion = android.os.Build.VERSION.SDK_INT
        if (started) {
            stopHeadService()
        } else if (currentApiVersion >= android.os.Build.VERSION_CODES.M) {
            OverlayWebList.clear()
            permissionHelper.setForceAccepting(true).request(Manifest.permission.SYSTEM_ALERT_WINDOW)
        } else {
            OverlayWebList.clear()
            startService(Intent(this, HeadService::class.java))
        }
    }

    protected val restoreButtonClick: View.OnClickListener = View.OnClickListener {
        if (!Session.restore) {
            if (!started) {
                val intent = Intent(this, HeadService::class.java)
                intent.action = HeadService.RESTORE
                startService(intent)
            } else {
                requestRestore()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initReceivers()
        requestStateUpdate()
        showBtn.setOnClickListener(showButtonClick)
        restoreBtn.setOnClickListener(restoreButtonClick)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    protected fun initReceivers() {
        val filter = IntentFilter(HeadService.START)
        filter.addAction(HeadService.STOP)
        filter.addAction(HeadService.NOTIFY_STATE)
        registerReceiver(broadcastReceiver, filter)
    }

    protected fun requestStateUpdate() {
        val intent = Intent()
        intent.action = HeadService.REQUEST_STATE
        sendBroadcast(intent)
    }

    protected fun requestRestore() {
        val intent = Intent()
        intent.action = HeadService.RESTORE
        sendBroadcast(intent)
    }

    protected fun stopHeadService() {
        val intent = Intent()
        intent.action = HeadService.SELF_STOP
        sendBroadcast(intent)
    }

    override fun onPermissionGranted(permissionName: Array<out String>) {
        startService(Intent(this, HeadService::class.java))
    }

    override fun onNoPermissionNeeded() {
        startService(Intent(this, HeadService::class.java))
    }

    override fun onPermissionDeclined(permissionName: Array<out String>) {
    }

    override fun onPermissionNeedExplanation(permissionName: String) {
    }

    override fun onPermissionPreGranted(permissionsName: String) {
        startService(Intent(this, HeadService::class.java))
    }

    override fun onPermissionReallyDeclined(permissionName: String) {
    }
}
