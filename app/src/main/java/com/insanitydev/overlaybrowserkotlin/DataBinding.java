package com.insanitydev.overlaybrowserkotlin;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class DataBinding {

    @BindingAdapter("image")
    public static void loadImageBitmap(ImageView imageView, Bitmap image) {
        imageView.setImageBitmap(image);
    }
}
