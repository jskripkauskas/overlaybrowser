package com.insanitydev.overlaybrowserkotlin

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Bitmap
import android.view.View

class TabViewModel(var tabItem: TabItem) : BaseObservable() {
    @Bindable
    var tabCloseClickListener: View.OnClickListener? = null

    @Bindable
    var tabBackgroundClickListener: View.OnClickListener? = null

    val screenshot: Bitmap
        @Bindable
        get() = tabItem.screenshot

    val title: String
        @Bindable
        get() = tabItem.title
}
