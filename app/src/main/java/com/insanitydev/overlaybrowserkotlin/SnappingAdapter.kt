package com.insanitydev.overlaybrowserkotlin

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.insanitydev.overlaybrowserkotlin.databinding.TabItemBinding

/**
 * Created by Julius.
 */
open class SnappingAdapter(private val items: MutableList<TabViewModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val screenshotView = LayoutInflater.from(parent.context).inflate(R.layout.tab_item, parent, false)
        return ItemViewHolder(screenshotView)
    }

    override fun onBindViewHolder(recyclerViewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = recyclerViewHolder as ItemViewHolder
        val tabItem: TabViewModel = items[position]
        viewHolder.binding.tab = tabItem
        viewHolder.itemView.tag = position
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun remove(selectedPosition: Int) {
        if (selectedPosition < items.size) {
            items.removeAt(selectedPosition)
            notifyItemRemoved(selectedPosition)
        }
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    protected inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: TabItemBinding

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }

}