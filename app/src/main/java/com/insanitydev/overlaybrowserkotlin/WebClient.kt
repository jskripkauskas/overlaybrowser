package com.insanitydev.overlaybrowserkotlin

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.overlay.view.*

object WebClient : WebViewClient() {
    override fun onLoadResource(view: WebView, url: String) {
        super.onLoadResource(view, url)
    }

    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
        hideKeyboardFrom(view.context, view)
    }

    override fun onPageFinished(view: WebView, url: String) {
        OverlayWebList.getCurrent().urlEdit.setText(url)
        OverlayWebList.getCurrent().rippleBackBtn.visibility = if (view.canGoBack()) View.VISIBLE else View.GONE

        val backList = view.copyBackForwardList()
        if (backList.size > 1) {
            val urlOld = backList.getItemAtIndex(backList.currentIndex - 1).url
            Session.replace(urlOld, url).save(view.context.applicationContext)
        } else if (OverlayWebList.size == 1 && !Session.restore) {
            Session.clearAddSavedSession(view.context, url)
        } else {
            Session.add(url).save(view.context.applicationContext)
        }

        super.onPageFinished(view, url)
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}