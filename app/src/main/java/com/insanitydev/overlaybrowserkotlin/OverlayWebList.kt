package com.insanitydev.overlaybrowserkotlin

import android.view.View
import java.util.*

object OverlayWebList : ArrayList<View>() {
    var currentTab: Int = 0

    fun getCurrent():View = this[currentTab]
}