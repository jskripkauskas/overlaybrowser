package com.insanitydev.overlaybrowserkotlin

import android.content.Context
import java.util.*

object Session {
    var restore: Boolean = false
    val urlList: ArrayList<String> = ArrayList()

    fun add(url: String): Session {
        if (!urlList.contains(url)){
            urlList.add(url)
        }
        return this
    }

    fun remove(url: String): Session {
        urlList.remove(url)
        return this
    }

    fun replace(urlOld: String, urlNew: String): Session {
        remove(urlOld)
        add(urlNew)
        return this
    }

    fun save(context: Context) {
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayPrefs), Context.MODE_PRIVATE)
        prefs.edit().putStringSet(context.getString(R.string.sessionKey), urlList.toSet()).apply()
    }

    fun getSavedSession(context: Context): MutableList<String> {
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayPrefs), Context.MODE_PRIVATE)
        val tempList = prefs.getStringSet(context.getString(R.string.sessionKey), urlList.toSet()).toMutableList()

        tempList.filterNot { urlList.contains(it) }.forEach { add(it) }
        return urlList
    }

    fun clearSavedSession(context: Context) {
        urlList.clear()
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayPrefs), Context.MODE_PRIVATE)
        prefs.edit().clear().apply()
    }

    fun clearAddSavedSession(context: Context, url: String) {
        urlList.clear()
        urlList.add(url)
        val prefs = context.getSharedPreferences(context.getString(R.string.overlayPrefs), Context.MODE_PRIVATE)
        prefs.edit().clear().putStringSet(context.getString(R.string.sessionKey), urlList.toSet()).apply()
    }
}