package com.insanitydev.overlaybrowserkotlin

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log

/**
 * Created by Julius.
 */
open class HeadService : Service() {
    protected val FOREGROUND_ID = 999
    protected lateinit var mHardwareKeyWatcher: HardwareKeyWatcher
    protected var mHeadLayer: HeadLayer? = null

    companion object {
        val START = "com.insanitydev.START"
        val CLOSE = "com.insanitydev.CLOSE"
        val STOP = "com.insanitydev.STOP"
        val SELF_STOP = "com.insanitydev.SELF_STOP"
        val NOTIFY_STATE = "com.insanitydev.NOTIFY_STATE"
        val REQUEST_STATE = "com.insanitydev.REQUEST_STATE"
        val RESTORE = "com.insanitydev.RESTORE"
    }

    protected val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == HeadService.REQUEST_STATE) {
                val intentSend: Intent = Intent()
                intentSend.action = HeadService.NOTIFY_STATE
                sendBroadcast(intentSend)
            } else if (intent?.action == HeadService.SELF_STOP) {
                stop()
            } else if (intent?.action == HeadService.RESTORE) {
                mHeadLayer?.restoreSession()
            }
        }
    }

    protected var hardwareKeyListener = object : HardwareKeyWatcher.OnHardwareKeysPressedListener {
        override fun onHomePressed() {
            if (mHeadLayer!!.isExpanded()) {
                mHeadLayer?.collapse()
            }
        }

        override fun onRecentAppsPressed() {
            if (mHeadLayer!!.isExpanded()) {
                mHeadLayer?.collapse()
            }
        }
    }

    override fun onCreate() {
        mHardwareKeyWatcher = HardwareKeyWatcher(this)
        mHardwareKeyWatcher.setOnHardwareKeysPressedListenerListener(hardwareKeyListener)
        mHardwareKeyWatcher.startWatch()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (mHeadLayer == null) {
            initHeadLayer()
        }

        val pendingIntent: PendingIntent = createPendingIntent()
        val notification: Notification = createNotification(pendingIntent)
        startForeground(FOREGROUND_ID, notification)

        if (intent != null && intent.action == CLOSE) {
            stop()
        } else if (intent != null && intent.action == RESTORE) {
            mHeadLayer?.restoreSession()
            initReceivers()
            informServiceStarted()
        } else {
            initReceivers()
            informServiceStarted()
        }
        return Service.START_STICKY
    }

    override fun onDestroy() {
        if (mHeadLayer != null) {
            destroyHeadLayer()
        }
        unregisterReceiver(broadcastReceiver)
        informServiceStopped()
        mHardwareKeyWatcher.stopWatch()
        stopForeground(true)
    }

    protected fun initReceivers() {
        val filter = IntentFilter(START)
        filter.addAction(HeadService.SELF_STOP)
        filter.addAction(HeadService.REQUEST_STATE)
        filter.addAction(HeadService.RESTORE)
        registerReceiver(broadcastReceiver, filter)
    }

    protected fun stop() {
        if (mHeadLayer != null) {
            destroyHeadLayer()
        }
        stopForeground(true)
        stopSelf()
    }

    protected fun informServiceStarted() {
        val intent: Intent = Intent()
        intent.action = HeadService.START
        sendBroadcast(intent)
    }

    protected fun informServiceStopped() {
        val intent: Intent = Intent()
        intent.action = HeadService.STOP
        sendBroadcast(intent)
    }

    protected fun initHeadLayer() {
        mHeadLayer = HeadLayer(this)
    }

    protected fun destroyHeadLayer() {
        mHeadLayer!!.destroy()
        mHeadLayer = null
    }

    protected fun createPendingIntent(): PendingIntent {
        val intent = Intent(this, HeadService::class.java)
        intent.action = CLOSE
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    protected fun createNotification(intent: PendingIntent): Notification {
        return Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.close_msg))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(intent)
                .build()
    }

}